#Tech 

* This is how you search for any item in a json/yaml output.

  ```shell
  yq '.. | (.port?, .securePort?)' file.yml
  jq '.. | (.port?, .securePort?)' file.json
  ```

* Find the siblings to a searched for value.

  ```shell
  yq '.. | (.port?) | select (. == "5432") | parent | ["name:" + .name, "port:" + .port, "proto:" + .protocol]' manifest.yaml
  ```