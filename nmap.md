#Tech 

- Map listening ports of a remote machine

	```bash
	sudo nmap -Pn $HOSTNAME
	```

* Get a list of ciphers for a host.

	```shell
	sudo nmap -sV --script ssl-enum-ciphers -p 443 <host>
	```