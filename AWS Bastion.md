## Terraform

You can create a terraform script to create a user and add an ssh key.

```json
resource "aws_security_group" "appgate_bastion" {
  name        = "${var.sg_prefix}-bastion"
  description = "Security group used by AppGate Controllers"
  vpc_id      = var.vpc_id

  tags = {
    Name  = "Appgate-Bastion"
    Group = "Appgate"
  }
}

resource "aws_security_group_rule" "bastion_ingress" {
  description              = "Allow inbound traffic"
  type                     = "ingress"
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
  cidr_blocks              = ["0.0.0.0/0"]
  security_group_id        = aws_security_group.appgate_bastion.id
}

resource "aws_instance" "bastion" {
  ami           = "ami-078a57d089cbfad4a"
  instance_type = "t2.micro"
  vpc_security_group_ids = [aws_security_group.appgate_bastion.id]

  user_data = <<EOF
#cloud-config
cloud_final_modules:
- [users-groups,always]
users:
  - name: renninger.brock
    groups: [ wheel ]
    sudo: [ "ALL=(ALL) NOPASSWD:ALL" ]
    shell: /bin/bash
    ssh-authorized-keys:
    - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDtTJky5/Zavc4ir+VpOBS/MU8WwcapbGUFYCNCsTvTqh1pABhVEdTKWDMqe6kZZTXvIMq7NLPsyN7V3EaLQwVoOHKPc88ZpuiUgTFD2Rtgwi3Lm24txvHOo76W69xcGXCX/PxJvqCbpBnSxhqm6gGL1lcjfgSb7J+C/Z0rWilKNomt4qjhlKVFCUZb02TsmcjLGY7RAxGtkq0OWJpsSTk7DuLWZcBKs6BZscLg4qc8pD2uFEYG+mhJfWpw23Bzo5P6x8/VOG459CDVRRqYbDLr8jVhlzCeSSpa3z6njRcCroqwpd9rvNy2j6ssNXjU0MS+4dGsHszFp4bjywfZPELl6jPA8oK7JxiUT2+Q4m+fkncszs1Kk3dq09NMH6l1t5ebWYR9MmaJADb2GKHFuXeS1dFkf6GU5drZwTZQIlFFKnpS67ij3SvNvDYEJXf2pArgN2YcnHICLwevdzcdLRAOtCNbBJfbhKGRpyuvDotqoqR8nDxtHK6a77cTZ4WlY1L0= user@test-machine
EOF

}
