#Research #Tech 

I'm starting out with the docker-compose route to get a feel. To get it started you have to log in to their private repo to pull the container. But I think the code is on GitHub so we may just build it by hand.

Pulling the containers from WSO2 repository, is interesting. All of the containers carry the same version, but they have different creation times.

Some are from weeks ago, another is 5 hours, and the last is 2 hours ago. Why are they building under the same version but obviously there are changes being made.

Is this going to be a problem?
- [2020-09-08 21:17:02,040]  INFO - CallHomeExecutor

  ``` console
   docker run -it \
     -p 9443:9443 \
     -p 8243:8243 \
     -p 8280:8280 \
     docker.wso2.com/wso2am
   ```

- Login:

  - https://localhost:9443/admin
  - https://localhost:9443/publisher - Create API starting point
  - https://localhost:9443/devportal - Review API endpoints
  - https://localhost:9443/carbon - KeyStore
  - admin admin

- Interesting facts
    - You can set "business plans" of 1K, 2K, 5K, unlimited requests per minute API Calls.
        - Which can be used on a per-version call.
    - You can set up access to Application keys (I'm not sure yet for what) and limit how often that Key is accessible.

  - It Looks like it is capable of doing blue/green deployments.


### Install to OSE 3.11 - Operator

wget files from git:
- https://github.com/wso2/k8s-api-operator/releases

1. Log in to OSE
1. unzip operator
1. Download the apicli https://wso2.com/api-management/tooling

   - This requires a login. :/

1. install operator
1. apply api-portal

    - This started the API service but did not include setup the webUI(?)
         - The UI is there but in a strange location
         - First it uses port 32001 from the outside redirected to 9443 internally.
         - Then you must have an entry in your host file to get to the right location.

1. Create an entry into your hosts file to redirect wso2apim to a master node.
1. Setup a route that does a secure passthrough
1. Then you can goto https://wso2apim:32001