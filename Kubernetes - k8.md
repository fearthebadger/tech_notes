#Tech 

> [!Note]
> Kubernetes -> OpenShift -> OKD

## Interaction Commands

* **$cmd**
	* `kubectl` => Kubernetes
	* `oc` => OpenShift

### Get certs

```bash
$cmd get csr
```

## port-forward

```bash
kubectl portforward svc/argocd-server -n argocd 9090:443
```