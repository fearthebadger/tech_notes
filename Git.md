# Git

## Create a local repo to push to

``` bash
git init --bare $PWD/repos/repo.git
```

## Check out a single directory from a git repo

``` bash
git init repo
cd repo
git remote add -f origin $PWD/repos/repo.git

git config core.sparseCheckout true

echo "some/dir/" >> .git/info/sparse-checkout

git pull
```
