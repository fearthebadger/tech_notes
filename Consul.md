#Tech 

## How to setup Consul

> [!NOTE]
> This is how you set up consul and talk to it.

1. Download the consul executable.

   <https://www.consul.io/downloads.html>

1. Create a service file

    ``` bash
    sudo vi /usr/lib/systemd/system/consul.service
    ```

    ``` config
    [Unit]
    Description=Consul Server
    Requires=network-online.target
    After=network-online.target
    
    [Service]
    Environment=GOMAXPROCS=2
    Restart=on-failure
    ExecStart=/usr/local/bin/consul agent -config-dir=/etc/consul.d
    ExecReload=/bin/kill -HUP $MAINPID
    KillSignal=SIGINT
    
    [Install]
    WantedBy=multi-user.target
    ```

1. Update the consul config

    ``` bash
    /etc/sysconfig/consul
    ```

    ``` config
    OPTIONS="-ui -server -client=0.0.0.0 -data-dir=/tmp/consul"
    ```

> [!ATTENTION]
> If you start consul then move it to a new IP you'll need to worry about your
> cluster connecting.  I had to remove the /tmp/consul dir, but I then lost
> all of my stored k/v's.
>
> - I'm not sure what part of that dir controls the conenction history
> that hangs after moving.

## Local connection to Consul

> [!Note]
> You need Consul 1.0.2 and Vault 0.9.1 or above.

- Set the environement variables:

    ``` bash
    export VAULT_ADDR="http://consul-server-1.<FQDN>:8200"
    export VAULT_TOKEN="<vault_token>"
    ```

- Also install pip hvac

    ``` bash
    pip install hvac
    ```

- Connect your client to the consul cluster so that you can get vault variables.

    ``` bash
    sudo vi /etc/consul.d/config.json
    ```

    ``` json
    {
        datacenter: dc1,
        encrypt: fdNhpTojJWOrWhmFqmNEQA==,
        encrypt_verify_outgoing: true,
        encrypt_verify_incoming: true,
        bind_addr: <systemIP>
        data_dir: /opt/consul,
        log_level: INFO,
        server: false,
        retry_join: [
          consul-server-1.<FQDN>
        ]
    }
    ```

    ``` bash
    sudo vi /usr/lib/systemd/system/consul.service
    ```

    ``` config
    [Unit]
    Description=Consul Server
    Requires=network-online.target
    After=network-online.target multi-user.target
    
    [Service]
    Environment=GOMAXPROCS=2
    Restart=on-failure
    ExecStart=/usr/bin/consul agent -config-dir=/etc/consul.d
    ExecReload=/bin/kill -HUP $MAINPID
    KillSignal=SIGTERM
    
    [Install]
    WantedBy=multi-user.target
    ```

- Restart the consul client

    ``` bash
    sudo systemctl daemon-reload
    sudo  systemctl enable consul
    sudo systemctl start consul
    ```
