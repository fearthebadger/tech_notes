#Tech 

## Docker/Podman login

```bash
aws ecr \
  get-login-password \
  --region us-gov-west-1 \
  | podman login \
    --username AWS \
    --password-stdin 763659020100.dkr.ecr.us-gov-west-1.amazonaws.com
```

* Create a landing place for the container

  ```bash
  aws ecr \
    create-repository \
    --repository-name q2a \
    --region us-gov-west-1
  ```

* Push the image to ECR

  ```bash
  podman push \
    763659020100.dkr.ecr.us-gov-west-1.amazonaws.com/q2a:v1.8.6
  ```

## Bastion Host

- [[AWS Bastion]]