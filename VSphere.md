#Tech 

## Upload to a DataStore

| Variable | Description | Example | 
|---|---|---|
| $DOMAIN | The login domain | `example.com` | 
| $USER | Your username | `renninger.brock` | 
| $PASSWORD | Your password | `password` | 
| $IMAGE | The data file name | `image.iso` | 
| $VCENTER | The fqdn of your VCENTER | `https://vcsa.dev.example.com` | 
| $DIR | Any sub directories in which you want to place the file. | `files/Dev` | 
| $DATACENTER | The name of the datacenter you are pushing to. | `Development` | 
| $DATASTORE | The name of the datastore for the files you are pushing. | `Datastore` | 

```bash
curl -kv \
  --user "$DOMAIN\\$USER:$PASSWORD" \
  -T @./$IMAGE \
  -X PUT \
  "$VCENTER/folder/$DIR/$IMAGE?dcPath=$DATACENTER&dsName=$DATASTORE"
```