## Generate SSL Certs

> [!NOTE]
>  This will generate certificates for you use in self signed applications.

1. Generate a unique private key (KEY)

    ``` bash
    sudo openssl genrsa -out test.key 2048
    ```

1. Generating a Certificate Signing Request (CSR)

    ``` bash
    sudo openssl req -new -key test.key -out test.csr
    ```

1. Creating a Self-Signed Certificate (CRT)

    ``` bash
    sudo openssl x509 -req -days 365 -in test.csr -signkey test.key -out test.crt
    ```

1. Append KEY and CRT to mydomain.pem

    ``` bash
    sudo cat test.key test.crt >> test.pem
    ```
