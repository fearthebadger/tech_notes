#Tech 

This has been a little problematic...

In order to get a container running with in a system you need to setup a license server. That was relatively easy to set up but the standalone, defaults to some things that are uncommon.

- It starts the license service using port `2002`.
- It opens the web interface to port `8080`.

When I was trouble shooting, I found out there are two versions of Parasoft with a pro version and a standard version. The issue is that they both use the same exact executable so when you go to look at the documentation you don't realize which version you are referencing. Even then they have terrible documentation. As an example, in one version `cpptestcli -listconfigs` vs the other version `cpptestcli -list-configs`. Ridiculous.

The people I have been talking to are so reliant on the GUI that they are having a hard time getting with it on the CLI.

``` config
cpptest.license.network.type=ls
cpptest.license.use_network=true
cpptest.license.network.edition=custom_edition
cpptest.license.custom_edition_features=C++test,Static Analysis,Flow Analysis,Unit Test,Coverage,Automation,DTP Publish,Desktop Command Line,AUTOSAR Rules,CWE Rules,HIC++ Rules,JSF Rules,MISRA Rules,MISRA C 2012 Rules,OWASP Rules,Security Rules,SEI CERT C Rules,SEI CERT C++ Rules

license.network.use.specified.server=true
license.network.host=192.168.56.106
license.network.port=8080
```

## Start License Server

1. Boot VM

1. Elevate to Root

   ``` console
   $ sudo su -
   ```

1. Start LS

   ``` console
   # cd /opt/parasoft
   # ./startLS.sh
   ```

## Test Code

   ``` console
   # ../cpptestcli -config "builtin://Recommended Rules" -compiler gcc_8-64 -trace make
   ```

## Quick fix for setting the date.

1. Login to LS

1. Elevate to Root

   ``` console
   $ sudo su -
   ```

1. Turn of `chronyd`

   ``` console
   # systemctl stop chronyd
   ```

1. Set date of system

   ``` console
   # timedatectl set-time 2021-01-01
   ```
