#Tech 

* When adding `--permanent` you must run the `--reload`

``` bash
sudo firewall-cmd --zone=public --permanent --add-port=6443/tcp
sudo firewall-cmd --reload
sudo firewall-cmd --list-all
```