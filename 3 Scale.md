#Research #Tech 

So this one is being shot down rather quickly.

This is a product that is tied into OpenShift rather heavily.

Its UI ties directly into the OpenShift UI.

- Which tells me there is little hope for it working outside of OSE.

The cost structure is prohibative.

- $750/month for 3 API endpoints.
- Talk to us for more than that.

It is clearly designed for large scale orginations that sell services to the API end points.

- They have mechanisms to charge per connection, all the time or after an initial set.
