#Tech 

## Scan Container

1. Create docker compose directory.

   ```bash
   mkdir anchore
   cd anchore
   ```

2. Pull down the docker compose file.

   ``` bash
   curl https://docs.anchore.com/current/docs/engine/quickstart/docker-compose.yaml > docker-compose.yaml
   ```

3. Start the Anchore container

   ``` bash
   docker-compose up -d # start the container services
   ```

4. Wait for all the services to start before trying to scan a container

   ``` bash
   sleep 300
   ```

5. Run Anchore against a container you have started.

   ``` bash
   docker-compose \
   exec \
   engine-api \
   anchore-cli image vuln \
   <image_id> \
   all
   ```