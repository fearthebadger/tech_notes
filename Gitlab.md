#Tech 

## Runner

* List runners

  ```
  # gitlab-runner list
  ```

* Unregister a runner

  ```
  # gitlab-runner unregister -t $TOKEN -u $URL
  ```

* Register a privileged runner

  ```
  # gitlab-runner register --docker-privileged
  ```

	* Create a semi-unique tag so only your pipelines have access.
	* Choose docker
	* Provide the base image you want to default to.
* Restart runner

  ```bash
  sudo gitlab-runner restart
  ```

## Update Config

```bash
sudo vi /etc/gitlab-runner/config.toml
  concurent = 4
  [[runner]]
    limit = 1
sudo systemctl restart gitlab-runner
```

## Setup RHEL-8 template

 1. Fix the [[LVM]]

 2. Fix the `/etc/bashrc` to not have the session stuff.

 3. change `/etc/selinux/config` to permissive (for now).

 4. Kill fapolicyd (for now).

    ```shell
    systemctl stop fapolicyd
    systemctl disable fapolicyd
    ```

 5. Fix the max_user_namespace

    ```bash
    vi /etc/sysctl.d/99-sysctl.conf
    ```

    ```plain
    user.max_user_namespace=31477
    ```

 6. Add gitlab-runner [[useradd, userdel]] 

 7. Change the hostname

    ```bash
    vi /etc/hostname
    ```

 8. Reboot

 9. Install docker and git

    ```bash
    subscription-manager register --auto-attach
    dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
    dnf install -y docker-ce git
    ```

10. Install the gitlab-runner

    ```bash
    curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/rpm/gitlab-runner_amd64-fips.rpm"
    rpm -i gitlab-runner_amd64-fips.rpm
    ```