#Tech  

__s2i__
: [source-to-image](https://github.com/openshift/source-to-image)

__[okd](https://www.okd.io/)__
: "An opensource version of RedHat's OpenShift"

This is some instructions on how to handle an interaction between s2i and OKD.

1. Build your s2i setup
    * Some helpful [instructions](https://blog.openshift.com/create-s2i-builder-image/).
1. Push s2i setup to a repository
    * example: `s2i-repo.git`
1. Have a separate repository for your running code.
    * example: `code.git`
1. Connect to your okd instance.

    ``` bash
    oc login <okd-master.server.fqdn>:<okd-master.server.port>
    ```

1. Build a new container based on your s2i setup.

    ``` bash
    oc new-build s2i-repo.git --name s2i-repo --source-secret='okd-secret'
    ```

1. Build a new container on top of your previous built container

    > __IMPORTANT:__ The {build_name}(tilde){code_repo} is neccesary.
    > It makes okd understand that you are building your code ontop of an already created
    > project.
 
    ``` bash
    oc new-build s2i-repo~code.git --name code --source-secret='okd-secret'
    ```

1. Start a new application based on final container

    ``` bash
    oc new-app code --name code-app
    ```

1. Expose the ports used by the container

    ``` bash
    oc expose svc/code-app
    ```
