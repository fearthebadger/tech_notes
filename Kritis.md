#Tech 

Kritis will look at containers before you run them, to then see if there are any issues with the container from a security stand point. It will identify CVE's for the container and reject if any found are not on an acceptance list. It also has the ability to validate all images used with some sort of a gpg key signature process. (to be determined)

```
helm install --set serviceNamespace=kritis-cluster kritis-cluster kritis-cluster-0.2.2.tgz
```

## Issue Found

[Kritis Issue 583](https://github.com/grafeas/kritis/issues/583)

Since I have been running with Kubernetes 1.19 this is the issue I have been running into. There is no solution to this unless we rebuild the preinstall container...