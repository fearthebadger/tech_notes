#Tech 

Get connection information from lsof

* Check all IPV4 connections

``` shell
sudo lsof -Pnl +M -i4
```

* Check all connections on `$PROTOCOL:$PORT`

``` shell
sudo lsof -i TCP:$PORT
sudo lsof -i UDP:$PORT
```