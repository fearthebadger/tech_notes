#Tech 
## Generate SSL Certs

> [!note] This will generate certificates for you use in self signed applications.

1. Generate a unique private key (KEY)

   ```bash
   openssl genrsa -out test.key 2048
   ```

2. Generating a Certificate Signing Request (CSR)

   ```bash
   openssl req -new -key test.key -out test.csr
   ```

3. Creating a Self-Signed Certificate (CRT)

   ``` bash
   openssl x509 -req -days 365 -in test.csr -signkey test.key -out test.crt
   ```

4. Append KEY and CRT to mydomain.pem

   ```bash
   cat test.key test.crt >> test.pem
   ```

## Get a list of ciphers

  ```shell
  for v in ssl2 ssl3 tls1 tls1_1 tls1_2; do
    for c in $(openssl ciphers 'ALL:eNULL' | tr ':' ' '); do
  	openssl s_client -connect <server>:<port> \
  	-cipher $c -$v < /dev/null > /dev/null 2>&1 && echo -e "$v:\t$c"
    done
  done
  ```