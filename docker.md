#Tech 

## Useful Links

* [Docker](https://www.docker.com/)
* [Docker Hub](https://www.dockerhub.com/)

## Random notes

* If you mount a volume `-v ${PWD}/vol:/vol` it will override the contents of the `/vol` directory inside the container.

## Archive Docker Containers and Images

* Export Docker Images

    ```bash
    docker save -o service.tar service_image
    tar czf service.tar.gz service.tar
    ```

* Export Docker Container

    ```bash
    docker export -o service.tar service_container
    tar czf service.tar.gz service.tar
    ```

* Import Docker Image

    ```bash
    tar xf service.tar.gz
    docker import service.tar service:latest
    ```

## Post-Install

1. Create a local docker group

    ```bash
    bash groupadd docker
    ```

2. Add user to docker group

    ```bash
    sudo usermod -aG docker $USER
    ```

3. Restart docker process

    ```bash
    sudo systemctl restart docker
    ```

4. Log out and in.

## [[podman#Build a Container from a running system]]

## Docker Compose (Version: 1.21.0\+)

> There was a big change made in version 1.21.0 that allows for and ssh connection instead of tcp. Removing the security vulnerability of an open port (2375).
>
> **You can ignore everything in the remote setup for this to work.**

### Local Commands (ssh)

> [!Note]
> There are two different ways of setting up your environment

* Single command (ssh)

    ```bash
    docker-compose -H "ssh://(user)@<host>" pull
    docker-compose -H "ssh://(user)@<host>" up
    docker-compose -H "ssh://(user)@<host>" down
    ```

* Environment (ssh)

    ```bash
    export DOCKER_HOST="ssh://(user)@<host>"

    docker-compose pull
    docker-compose up
    docker-compose down
    ```

  > [!Note]
  > This will help with some of the docker compose things that come up.

## Docker Compose (Versions older than 1.21.0)

`docker-compose` can be used to talk to remote servers to run.

### Remote Setup

1. Modify the service file `/usr/lib/systemd/system/docker.service`

   ```service
   ...
   
   # Adding -H 0.0.0.0:2375 to the ExecStart command.
   ExecStart=/usr/bin/dockerd -H fd:// -H 0.0.0.0:2375 --containerd=/run/containerd/containerd.sock
   
   ...
   ```

2. Reload and restart the docker service

   ```console
   # systemctl daemon reload
   # systemctl restart docker
   ```

3. Open the firewall

   ```console
   # firewall-cmd --zone=public --add-port=2375/tcp --permanent
   # firewall-cmd --reload
   ```

### Local Commands (tcp)

> **NOTE:**
> There are two different ways of setting up your environment

* Single command (tcp)

  ```console
  $ docker-compose -H "tcp://<host>:2375" pull
  $ docker-compose -H "tcp://<host>:2375" up
  $ docker-compose -H "tcp://<host>:2375" down
  ```

* Environment (tcp)

  ```console
  $ export DOCKER_HOST="tcp://<host>:2375"
  
  $ docker-compose pull
  $ docker-compose up
  $ docker-compose down
  ```

## Inserting a layer taken from a donor image

<https://github.com/moby/moby/issues/43522>

> Notes:
>
> * I am unsure if this is a desired outcome, a bug, or even a concern, but there is a way that you can take a layer from an alternate docker build image and insert it into the middle of an alternate image. This insertion will present as a layer in the receiving image just as if it had been included in the original Dockerfile.
>
> * I have only worked with exporting an image (`docker save`), modifying it, and then importing this new image (`docker load`). I have not dived deeper into all the potential variations that can be accomplished by this strategy.
>
> * To calm my immediate fears, I found that the modified image has a different `IMAGE ID` than the original image.

### How to accomplish this:

1. Setup environment

   * Create directory structure

     ```shell-session
     mkdir -p orig/image donor/image
     ```

2. Build a base image to start from

   * Create a new `$EDITOR orig/Dockerfile`

     ```Dockerfile
     FROM ubuntu:latest
     
     RUN apt update && apt install -y cowsay
     ```

   * Build the new image

     ```shell-session
     docker build -t test:orig orig/
     ```

   * Export image to local file

     ```shell-session
     docker save test:orig > test_orig.tar
     tar -xf test_orig.tar -C orig/image/
     ```

3. Create a second image based off of that original

   * Create a new `$EDITOR donor/Dockerfile`

     ```Dockerfile
     FROM test:orig
     
     RUN /usr/games/cowsay "I'm new here and should not exist" > /cowsay.txt
     ```

   * Build the new image

     ```shell-session
     docker build -t test:donor .
     ```

   * Export image to local file

     ```shell-session
     docker save test:donor > test_donor.tar
     tar xf test_donar.tar -C donor/image/
     ```

4. Find the information needed to insert the layers

   * Get `sha256` hash of `test:donor`

     ```shell-session
     docker inspect -f "{{ range .RootFS.Layers }}{{ println . }}{{end}}" test:donor
     ```

     * Record the last hash in the list

       <donor_hash> \= `sha256:258c504b1160276a3a4c……`

   * Find the folder hash of the donor layer in `donor/image/` and copy to the `orig/image`.

     ```shell-session
     cat donor/image/manifest.json
     ```

     * Record the last layer hash in the list, omitting the `/layer.tar`.

       <donor_folder_hash> \= `7a1942eded12b4142f27a4d8……`

     * Copy that folder over to the original directory

       ```shell-session
       cp -rp donor/image/<donor_folder_hash>/ orig/image/
       ```

   * Find the head of the original image

     ```shell-session
     cat orig/image/manifest.json
     ```

     * Record the last layer hash in the list, omitting the `/layer.tar`

       <orig_head_hash> \= `822ff5037aa……`

   * Find the parent of the head layer

     ```shell-session
     cat orig/image/<orig_head_hash>/json
     ```

     * Record the `parent:` value hash

       <orig_parent_hash> \= `ded12b4142f27a4d8b……`

5. Insert the new layer into the stack

   * Change directories to `orig/image`

     ```shell-session
     cd orig/image
     ```

   * Insert the folder hash into the manifest.json

     ```shell-session
     $EDITOR manifest.json 
     ```

     * Add `“<donor_folder_hash>/layer.tar”, ` to the **second to last** entry of the array.

   * Insert the new layer hash into the image json file. (This is the only json file in this base dir with a hash for a file name).

     ```shell-session
     $EDITOR <file_hash>.json
     ```

     * At the end of the line/file in the layers section add `”<donor_hash>”,` to the **second to last entry** of the `diff_ids` array.

   * Point the donor layer’s parent at the parent of the head layer.

     ```shell-session
     cd <donor_folder_hash>/
     cp -p ../<orig_parent_hash>/json .
     $EDITOR json
     ```

     * Insert a new `id:` before the existing `id`.

       `”id”:”<donor_folder_hash>”`

     * Change the now second `id:` to `parent:` then remove the now second `parent:` id and value if it exists.

   * Point the parent of head image to the new layer.

     ```shell-session
     cd ../<orig_head_hash>/
     $EDITOR json
     ```

     * Change the `parent` value to the `<donor_folder_hash>`

   * Go back a directory and create a new image tar

     ```shell-session
     cd ..
     tar -cf ../../test_new.tar .
     ```

6. Remove the old images and load the newly modified image

   ```shell-session
   docker rmi test:orig test:donor ubuntu:latest
   docker load < ../../test_new.tar
   ```

7. Validate that the image was changed.

   ```shell-session
   docker run -it --rm --entrypoint "/bin/bash" test:orig
   # cat /cowsay.txt
   ```

### Questions

* could you push layers into container for with out having yum in the OS.
* further thought could you peel back N top layers to get to a single layer deeper in the stack?