#Tech 

This will help with dealing with git submodules

* Clone all submodules with the initial clone.

    ``` bash
    git clone --recursive repo.git
```

* Clone submodule after already cloning a repo

    ``` bash
    cd repo_dir/
    git submodule add sub-repo.git sub_repo_dir/
    git submodule update --init --recursive
    ```

* Commit a specific submodule branch/tag/commit to current project

    ``` bash
    cd repo_dir/sub_repo_dir/
    git checkout v1.0.0
    cd ../
    git add .git/module/*
    git commit
    git push
    ```

* Pull all updates including submodule updates.

    ``` bash
    git pull --recursive-modules
    ```

* If you want to make things easier to clone and pull you can setup aliases.
    ```bash
    
    sudo vi ~/.gitconfig
    ```

    ``` conf
    ...
    [alias]
    ...
    pl = pull --recursive-modules
    cl = clone --recursive
    ...
    ```