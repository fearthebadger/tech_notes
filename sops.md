#Tech 

Encrypt or Decrypt data using AWS keys.

## Encrypt

* Create a `.sops.yaml` file

  ```bash
  cat .sops.yaml
  creation_rules:
    - path_regex: .*/prod/.*
      kms: 'arn:aws-us-gov:kms:us-gov-west-1:763659020100:key/cce80fa5-bb0a-4767-b806-9a9044da1a6a'
  ```

* Create your file that you want to encrypt

  ```bash
  cat test.yaml
  password: test
  ```

* Encrypt the file

  ```bash
  sops -e test.yaml > test.enc.yaml
  ```

## Decrypt

```bash
export AWS_DEFAULT_PROFILE=solute-gov
sops -d test.enc.yaml
```