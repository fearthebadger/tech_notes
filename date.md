#Tech 

- Get today's date

  ``` bash
  date +'%Y-%m-%d'
  ```

- Get a date from `N` days ago.

  ``` bash
  date +'%Y-%m-%d' -d "52 days ago"
  ```