#Tech 

## Headless by Default

Set a VM to start in headless by default.

> [!Note]
> The VM must be shutdown for this to work

### Windows

* Open elevated command prompt

  ```
  Windows Key 
  > cmd 
  >> (right-click) Command Prompt 
  >>> select Run as Administrator 
  ```

* Change directories to virtualbox's home

  ```powershell
  cd "Program Files\Oracle\VirtualBox" 
  ```

* Set `defaultfrontend`

  ```powershell
  VBoxManage.exe modifyvm $VMName --defaultfrontend headless
  ```

### Linux

* Become root

  ``` bash
  sudo su - 
  ```

* Set `defaultfrontend`

  ```bash
  sudo vboxmanage modifyvm $VMName --defaultfrontend headless
  ```