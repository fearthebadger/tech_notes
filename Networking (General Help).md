#Tech 

> [!Note]
> These are tools to use when dealing with a network

## Tools to install
  - net-tools, lsof

## List all listening ports

``` bash
sudo netstat -tulpn
```

## Show status of your network interface.

``` bash
sudo nmcli c s (interface)
```

## Get everything running on a specific port number.

``` bash
sudo ss -aln | grep $PORT
```

## Get connection information from `lsof`

- Check all connection on IPV4

    ``` bash
    sudo lsof -Pnl +M -i4
    ```

- Check all connections on `$PROTOCOL:$PORT`

    ``` bash
    sudo lsof -i TCP:$PORT
    sudo lsof -i UDP:$PORT
    ```

- Map listening ports of a remote machine

    ``` bash
    sudo nmap -Pn $HOSTNAME
    ```

## Work with the `firewalld`

- Printout the rules

    ``` bash
    sudo firewallcmd list-all
    ```

- Permanently add firewall rules

    ``` bash
    sudo firewall-cmd --zone=public --add-port=1234/tcp --permanent
    sudo firewall-cmd --reload
    ```