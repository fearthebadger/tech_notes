#Tech 

After working with [Ansible](https://wwww.ansible.com/) these are things out of the ordinary that may be of use.

## Built-In Variables

> ***NOTE:*** Ansible has built-in variables that can be used without defining them.

* IP Address based on the network interface

  * This will return the ipv4 address of the the interface specified.

  ```yaml
  # interface = ens192
  "{{ ansible_ens192.ipv4.address }}"
  ```

## Passing Variables to Meta Dependencies

There are times where you have a custom role that will layer on top of a generic role via a role meta dependency. The dependency role may contain default variables that don't work with your custom role. There may be a need to pass custom variables to the generic role before running your layer.

### Diff Example

``` yaml
# generic-role/defaults/main.yml
# ...
postgresql_version: 11.2.1
# ...
```

``` yaml
# custom-role/defaults/main.yml
# ...
postgresql_version: 9.6.12
# ...
```

### Solution

In order to accomplish this you need to pass the custom-role defaults variables to the meta.