#Tech 

## Update an LVM when a new hard drive is added.

1. Partition the new addition

   ```bash
   sudo fdisk /dev/sda
   ```

   ```shell
   n
   enter
   enter
   enter
   t
   (select the partition you just created)
   31
   p
   w
   ```

   * Set the type to Linux LVM `31`

2. Review what logical volumes are on the system already.

   * Find the `LV Path` you want and the `VG Name` for that path. 

   ```shell
   sudo vgdisplay -v
   ```

3. Extend the LVM

   ```shell
   sudo pvcreate /dev/sda4
   sudo vgextend $VG_NAME /dev/sda4
   sudo lvextend $LV_PATH /dev/sda4
   sudo xfs_growfs $LV_PATH
   ```