#Tech 

## GitLab Cli

```bash
glab auth login
? What GitLab instance do you want to log into? GitLab Self-hosted Instance
? GitLab hostname: gitlab-ssh.apps.solute.us
? API hostname: gitlab.apps.solute.us
- Logging into gitlab-ssh.apps.solute.us

Tip: you can generate a Personal Access Token here https://gitlab-ssh.apps.solute.us/-/profile/personal_access_tokens
The minimum required scopes are 'api' and 'write_repository'.
? Paste your authentication token: ********************
? Choose default git protocol SSH
? Choose host API protocol HTTPS
- glab config set -h gitlab-ssh.apps.solute.us git_protocol ssh
✓ Configured git protocol
- glab config set -h gitlab-ssh.apps.solute.us api_protocol https
✓ Configured API protocol
✓ Logged in as renninger.brock
```