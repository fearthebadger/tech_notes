#Tech 

* <https://github.com/moby/buildkit>

```bash
export DOCKER_BUILDKIT=1
docker build -t test .
```

* There is a way to pass SSH into the container

  ```bash
  docker build --ssh
  ```

  ```bash
  RUN --mount=type=ssh git clone git@example.com:/project.git
  ```

* I looked into this and it just doesn’t make any sense to work with at the moment.